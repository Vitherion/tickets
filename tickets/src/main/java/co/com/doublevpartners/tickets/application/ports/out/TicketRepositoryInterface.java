package co.com.doublevpartners.tickets.application.ports.out;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.com.doublevpartners.tickets.application.dtos.TicketDTO;

public interface TicketRepositoryInterface {
	
	TicketDTO crearTicket(TicketDTO ticket);
	
	TicketDTO obtenerTicketPorId(String id);
	
	TicketDTO actualizarTicket(TicketDTO ticket);
	
	void eliminarTicket(String id);

	Page<TicketDTO> listaTicketsPaginados(Pageable pageable);
	
}
