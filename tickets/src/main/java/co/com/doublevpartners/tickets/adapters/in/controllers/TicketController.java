package co.com.doublevpartners.tickets.adapters.in.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.com.doublevpartners.tickets.application.dtos.TicketDTO;
import co.com.doublevpartners.tickets.application.ports.in.TicketServiceInterface;
import org.springframework.data.domain.Page;

@RestController
@RequestMapping("api/v1/tickets")
public class TicketController {
	
	@Autowired
	private TicketServiceInterface ticketServiceInterface;
	
	@PostMapping
	public ResponseEntity<TicketDTO> crearTicket(@RequestBody TicketDTO ticket) {
		try {
			TicketDTO ticketCreado = ticketServiceInterface.crearTicket(ticket);
			return ResponseEntity.status(HttpStatus.CREATED).body(ticketCreado);
		} catch (Exception e) {
	        // Manejo de la excepción
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	    }
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<TicketDTO> obtenerTicketPorId(@PathVariable String id) {
		try {
			TicketDTO ticket = ticketServiceInterface.obtenerTicketPorId(id);
	        if (ticket != null) {
	            return ResponseEntity.ok(ticket);
	        } else {
	            return ResponseEntity.notFound().build();
	        }
	    } catch (Exception e) {
	        // Manejo de la excepción
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	    }
	}
	
	@PutMapping
	public ResponseEntity<TicketDTO> actualizarTicket(@RequestBody TicketDTO ticketDTO) {
	    try {
	    	TicketDTO ticketActualizado = ticketServiceInterface.actualizarTicket(ticketDTO);
	        if (ticketActualizado != null) {
	            return ResponseEntity.ok(ticketActualizado);
	        } else {
	            return ResponseEntity.notFound().build();
	        }
	    } catch (Exception e) {
	        // Manejo de la excepción
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	    }
	}
	
	@DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminarTicket(@PathVariable String id) {
        try {
            ticketServiceInterface.eliminarTicket(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            // Manejo de la excepción
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
	

    @GetMapping("/listar/{numeroPagina}/{tamanoPagina}")
    public Page<TicketDTO> listaTicketsPaginados(@PathVariable int numeroPagina,@PathVariable int tamanoPagina) {
    	try {
            return ticketServiceInterface.listaTicketsPaginados(numeroPagina, tamanoPagina);
        } catch (IllegalArgumentException e) {
            // Excepción lanzada si los valores de paginación son inválidos (por ejemplo, números negativos)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Valores de paginación inválidos", e);
        } catch (Exception e) {
            // Otras excepciones no esperadas
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Ocurrió un error en el servidor", e);
        }
    }

}
