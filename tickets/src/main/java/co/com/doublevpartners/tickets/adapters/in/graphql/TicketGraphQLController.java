package co.com.doublevpartners.tickets.adapters.in.graphql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import co.com.doublevpartners.tickets.application.dtos.TicketDTO;
import co.com.doublevpartners.tickets.application.ports.in.TicketServiceInterface;

@Controller
public class TicketGraphQLController {
	
	@Autowired
	private TicketServiceInterface ticketServiceInterface;
	
	@MutationMapping
	public TicketDTO crearTicket(@Argument TicketRequest ticketRequest) {
		try {
			TicketDTO ticketDTO = TicketRequestMapper.requestToDTO(ticketRequest);
			return ticketServiceInterface.crearTicket(ticketDTO);
		} catch (Exception e) {
	        // Manejo de la excepción (puedes lanzar una excepción específica de GraphQL si lo deseas)
	        throw new RuntimeException("Ocurrió un error al crear el ticket");
	    }
	}
	
	@QueryMapping
	public TicketDTO obtenerTicketPorId(@Argument String id) {
		try {
			TicketDTO ticket = ticketServiceInterface.obtenerTicketPorId(id);
	        if (ticket != null) {
	            return ticket;
	        } else {
	            // Devuelve null si no se encuentra el ticket (puedes lanzar una excepción específica de GraphQL si lo deseas)
	            return null;
	        }
	    } catch (Exception e) {
	        // Manejo de la excepción (puedes lanzar una excepción específica de GraphQL si lo deseas)
	        throw new RuntimeException("Ocurrió un error al obtener el ticket");
	    }
	}
	
	@MutationMapping
	public TicketDTO actualizarTicket(@Argument TicketRequest ticketRequest) {
	    try {
	    	TicketDTO ticketDTO = TicketRequestMapper.requestToDTO(ticketRequest);
	    	TicketDTO ticketActualizado = ticketServiceInterface.actualizarTicket(ticketDTO);
	        // En GraphQL no se necesita comprobar si el ticket fue encontrado ya que el tipo de retorno es nullable
	        return ticketActualizado;
	    } catch (Exception e) {
	        // Manejo de la excepción (puedes lanzar una excepción específica de GraphQL si lo deseas)
	        throw new RuntimeException("Ocurrió un error al actualizar el ticket");
	    }
	}
	
	@MutationMapping
	public void eliminarTicket(@Argument String id) {
        try {
            ticketServiceInterface.eliminarTicket(id);
        } catch (Exception e) {
            // Manejo de la excepción (puedes lanzar una excepción específica de GraphQL si lo deseas)
            throw new RuntimeException("Ocurrió un error al eliminar el ticket");
        }
    }
	
	@QueryMapping
    public Page<TicketDTO> listaTicketsPaginados(@Argument int numeroPagina, @Argument int tamanoPagina) {
    	try {
            return ticketServiceInterface.listaTicketsPaginados(numeroPagina, tamanoPagina);
        } catch (IllegalArgumentException e) {
            // Excepción lanzada si los valores de paginación son inválidos (por ejemplo, números negativos)
            throw new IllegalArgumentException("Valores de paginación inválidos");
        } catch (Exception e) {
            // Otras excepciones no esperadas
            throw new RuntimeException("Ocurrió un error en el servidor");
        }
    }

}
