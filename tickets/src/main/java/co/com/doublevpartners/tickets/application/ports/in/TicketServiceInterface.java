package co.com.doublevpartners.tickets.application.ports.in;

import org.springframework.data.domain.Page;

import co.com.doublevpartners.tickets.application.dtos.TicketDTO;

public interface TicketServiceInterface {
	
	TicketDTO crearTicket(TicketDTO ticket);
	
	TicketDTO obtenerTicketPorId(String id);
	
	TicketDTO actualizarTicket(TicketDTO ticket);
	
	void eliminarTicket(String id);
	
	Page<TicketDTO> listaTicketsPaginados(int numeroPagina, int tamanoPagina);

}
