package co.com.doublevpartners.tickets.adapters.in.graphql;

import org.modelmapper.ModelMapper;

import co.com.doublevpartners.tickets.application.dtos.TicketDTO;

public class TicketRequestMapper {
	
	private static ModelMapper modelMapper = new ModelMapper();
	
	public static TicketDTO requestToDTO(TicketRequest ticketRequest) {
        return modelMapper.map(ticketRequest, TicketDTO.class);
    }
	
	public static TicketRequest dtoToRequest(TicketDTO ticketDTO) {
		return modelMapper.map(ticketDTO, TicketRequest.class);
	}

}
