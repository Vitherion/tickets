package co.com.doublevpartners.tickets.adapters.out.persistence.mongoDB;

import org.modelmapper.ModelMapper;

import co.com.doublevpartners.tickets.application.dtos.TicketDTO;

public class TicketEntityMapper {
	
	private static ModelMapper modelMapper = new ModelMapper();
	
	public static TicketDTO entityToDTO(Ticket ticket) {
        return modelMapper.map(ticket, TicketDTO.class);
    }
	
	public static Ticket dtoToEntity(TicketDTO ticketDTO) {
		return modelMapper.map(ticketDTO, Ticket.class);
	}

}
