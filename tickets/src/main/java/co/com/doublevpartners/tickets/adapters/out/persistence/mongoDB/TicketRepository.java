package co.com.doublevpartners.tickets.adapters.out.persistence.mongoDB;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import co.com.doublevpartners.tickets.application.dtos.TicketDTO;
import co.com.doublevpartners.tickets.application.ports.out.TicketRepositoryInterface;

public interface TicketRepository extends MongoRepository<Ticket, String>, TicketRepositoryInterface {
	
	default TicketDTO crearTicket(TicketDTO ticketDTO) {
		Ticket ticket = TicketEntityMapper.dtoToEntity(ticketDTO);
		return Optional.ofNullable(save(ticket))
                .map(savedTicket -> TicketEntityMapper.entityToDTO(savedTicket))
                .orElse(null);
	}
	
	default TicketDTO obtenerTicketPorId(String id) {
		Optional<Ticket> ticketOptional = findById(id);
	    return ticketOptional.isPresent() ? TicketEntityMapper.entityToDTO(ticketOptional.get()) : null;
	}
	
	default TicketDTO actualizarTicket(TicketDTO ticketDTO) {

		Optional<Ticket> ticketOptional = findById(ticketDTO.getId());
		if (!ticketOptional.isPresent()) {
			return null;
		}
		
		Ticket ticket = TicketEntityMapper.dtoToEntity(ticketDTO);
	    return Optional.ofNullable(save(ticket))
	                   .map(ticketActualizado -> TicketEntityMapper.entityToDTO(ticketActualizado))
	                   .orElse(null);
	}
	
	default void eliminarTicket(String id) {
		deleteById(id);
	}
	
	default Page<TicketDTO> listaTicketsPaginados(Pageable pageable){
		Page<Ticket> tickets = findAll(pageable);
        return tickets.map(ticket -> TicketEntityMapper.entityToDTO(ticket));

	}
	
}
