package co.com.doublevpartners.tickets.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import co.com.doublevpartners.tickets.application.dtos.TicketDTO;
import co.com.doublevpartners.tickets.application.ports.in.TicketServiceInterface;
import co.com.doublevpartners.tickets.application.ports.out.TicketRepositoryInterface;

@Service
public class TicketService implements TicketServiceInterface{
	
	@Autowired
	private TicketRepositoryInterface repository;
	
	public TicketDTO crearTicket(TicketDTO ticket) {
		return repository.crearTicket(ticket);
	}
	
	public TicketDTO obtenerTicketPorId(String id) {
		return repository.obtenerTicketPorId(id);
	}
	
	public TicketDTO actualizarTicket(TicketDTO ticket) {
		return repository.actualizarTicket(ticket);
	}
	
	public void eliminarTicket(String id) {
		repository.eliminarTicket(id);
	}
	
	public Page<TicketDTO> listaTicketsPaginados(int numeroPagina, int tamanoPagina) {
        Pageable pageable = PageRequest.of(numeroPagina, tamanoPagina);
        return repository.listaTicketsPaginados(pageable);
    }

}
